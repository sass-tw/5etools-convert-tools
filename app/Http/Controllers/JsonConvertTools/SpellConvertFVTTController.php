<?php

namespace App\Http\Controllers\JsonConvertTools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class SpellConvertFVTTController extends Controller
{
    const LoadSpelldata = [
        "spelldata/sass-5e-packge.spells.json",
    ];

    //
    public function spellJsonGen()
    {
        $alljson = [];
        foreach ($this::LoadSpelldata as $file) {
            $json = Storage::disk('local')->get($file);
            $alljson = array_merge(
                $alljson,
                $this->SpellArrayStructure(json_decode($json, true))
            );
        }
        $alljson = json_encode($alljson, JSON_UNESCAPED_UNICODE);
        Storage::disk('local')->put('spells.json', $alljson);

    }

    private function SpellArrayStructure($Json)
    {
        $MMSpellArrayStructure = [
            "label" => "法術"
        ];
        foreach ($Json["entries"] as $Key =>$Array) {
            $MMSpellArrayStructure["entries"][$Key] = array(
                "name" => "{$Array["name"]} $Key",
                "description" => $Array["description"]
            );
        }
//        dd($MMSpellArrayStructure);
        return $MMSpellArrayStructure;
    }
}
