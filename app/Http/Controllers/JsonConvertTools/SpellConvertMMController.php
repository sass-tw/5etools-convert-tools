<?php

namespace App\Http\Controllers\JsonConvertTools;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class SpellConvertMMController extends Controller
{
    const PCclassMatch = [
        "BARBARIAN" => "野蠻人",
        "BARD" => "吟遊詩人",
        "CLERIC" => "牧師",
        "DRUID" => "德魯伊",
        "FIGHTER" => "戰士",
        "MONK" => "武僧",
        "PALADIN" => "聖騎士",
        "RANGER" => "遊俠",
        "ROGUE" => "盜賊",
        "SORCERER" => "術士",
        "WARLOCK" => "契術師",
        "WIZARD" => "法師"
    ];

    const LoadSpelldata = [
        "spelldata/spells-phb.json",
        "spelldata/spells-xge.json",
        "spelldata/spells-ai.json",
        "spelldata/spells-egw.json",
        "spelldata/spells-tce.json",
    ];

    //
    public function spellJsonGen()
    {
        $alljson = [];
        foreach ($this::LoadSpelldata as $file) {
            $json = Storage::disk('local')->get($file);
            $alljson = array_merge(
                $alljson,
                $this->MMSpellArrayStructure(json_decode($json, true))
            );
        }
        $alljson = json_encode($alljson);
        Storage::disk('local')->put('spells.json', $alljson);

    }

    private function MMSpellArrayStructure($Json)
    {
        $MMSpellArrayStructure = [];
        foreach ($Json["spell"] as $Array) {
//            dd($Array);
            $classOutput = [];
            if (isset($Array["classes"]["fromClassList"])) {
                foreach ($Array["classes"]["fromClassList"] as $ClassArray) {
                    $UpperName = strtoupper($ClassArray["name"]);
                    if (isset($this::PCclassMatch[$UpperName])) {
                        $classOutput[] = $this::PCclassMatch[$UpperName];
                    }
                }
            } elseif (isset($Array["classes"]["fromClassListVariant"])) {
                foreach ($Array["classes"]["fromClassListVariant"] as $ClassArray) {
                    $UpperName = strtoupper($ClassArray["name"]);
                    if (isset($this::PCclassMatch[$UpperName])) {
                        $classOutput[] = $this::PCclassMatch[$UpperName];
                    }
                }
            } elseif (isset($Array["classes"]["fromSubclass"])) {
                foreach ($Array["classes"]["fromSubclass"] as $ClassArray) {
                    $UpperName = strtoupper($ClassArray["class"]["name"]);
                    if (isset($this::PCclassMatch[$UpperName])) {
                        $classOutput[] = $this::PCclassMatch[$UpperName];
                    }
                }
            } else {
                dd($Array, "KEY MISS");
            }


            if (isset($Array["ENG_name"])) {
                $MMSpellArrayStructure[$Array["ENG_name"]] = array(
                    "name" => "{$Array["name"]} {$Array["ENG_name"]}",
                    "damage" => 1,
                    "multitarget" => false,
                    "level" => intval($Array["level"]),
                    "levelDisplay" => "",
                    "class" => $classOutput,
                    "srd" => true,
                    "custom" => false
                );
            } else {
                dd($Array, "KEY MISS");
            }


        }
        return $MMSpellArrayStructure;
    }
}
