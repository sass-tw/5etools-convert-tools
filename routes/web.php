<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
});



use App\Http\Controllers\JsonConvertTools\SpellConvertMMController;
use App\Http\Controllers\JsonConvertTools\SpellConvertFVTTController;
Route::get('/SpellConvertMM/spellJsonGen', [SpellConvertMMController::class, 'spellJsonGen']);
Route::get('/SpellConvertFVTT/spellJsonGen', [SpellConvertFVTTController::class, 'spellJsonGen']);
